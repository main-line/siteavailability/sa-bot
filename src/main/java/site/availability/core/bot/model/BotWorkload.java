package site.availability.core.bot.model;

import lombok.Data;

@Data
public class BotWorkload {
  private String url;
  private String redirect;
}
