package site.availability.core.bot.poll;

import com.amazonaws.services.s3.AmazonS3;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.time.Instant;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.Resource;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import site.availability.core.bot.model.WebsiteAvailability;
import site.availability.core.bot.queue.BotQueues;

@Component
@Slf4j
public class AvailabilityPersistencePoller {

  @Resource
  private AmazonS3 amazonS3;
  @Resource
  private ObjectMapper objectMapper;

  @Value("${sa.bucket.name}")
  @Setter
  private String bucketName;

  @Scheduled(fixedDelay = 60000)
  public void saveResultsToDataBase() throws JsonProcessingException {
    List<WebsiteAvailability> websiteAvailabilities = new ArrayList<>();
    BotQueues.WEB_CRAWL_RESPONSE_QUEUE.drainTo(websiteAvailabilities);

    if (!websiteAvailabilities.isEmpty()) {
      Instant now = Instant.now();
      ZonedDateTime zonedDateTime = now.atZone(ZoneId.systemDefault());
      String key = String.format("out/%s/%s/%s/%s.json",
          zonedDateTime.getMonth().getValue(),
          zonedDateTime.getDayOfMonth(),
          zonedDateTime.getHour(), now.toString());
      log.info("Persisting website availabilites Size : {}", websiteAvailabilities.size());
      amazonS3.putObject(bucketName, key, objectMapper.writeValueAsString(websiteAvailabilities));
    }
  }

}
