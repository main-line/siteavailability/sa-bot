package site.availability.core.bot.queue;

import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;
import site.availability.core.bot.model.WebsiteAvailability;

public class BotQueues {

  public static BlockingQueue<WebsiteAvailability> WEB_CRAWL_RESPONSE_QUEUE = new ArrayBlockingQueue<>(
      1000000);
}
