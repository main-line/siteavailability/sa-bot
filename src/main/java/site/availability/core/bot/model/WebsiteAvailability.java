package site.availability.core.bot.model;


import java.time.Instant;
import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class WebsiteAvailability {

  private String url;
  private String redirect;
  private Instant start;
  private Integer code;
}
