package site.availability.core.bot.processor;


import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.junit.MockitoJUnitRunner;
import site.availability.core.bot.config.HttpClientConfig;
import site.availability.core.bot.model.BotWorkload;
import site.availability.core.bot.model.WebsiteAvailability;
import site.availability.core.bot.queue.BotQueues;

@RunWith(MockitoJUnitRunner.class)
public class WebCrawlerTest {

  private WebCrawler webCrawler = new WebCrawler();

  @Test
  public void crawl() throws InterruptedException {
    webCrawler.setAsyncHttpClient(new HttpClientConfig().asyncHttpClient());

    BotWorkload botWorkload = new BotWorkload();
    botWorkload.setUrl("google.com");
    botWorkload.setRedirect("http://www.google.com");

    webCrawler.crawl(Arrays.asList(botWorkload));

    Thread.sleep(2000);
    List<WebsiteAvailability> output = new ArrayList<>();
    BotQueues.WEB_CRAWL_RESPONSE_QUEUE.drainTo(output);
    Assert.assertEquals(output.get(0).getUrl(), "google.com");
    Assert.assertEquals(200, (int) output.get(0).getCode());
  }
}
