package site.availability.core.bot.consumer;

import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.event.S3EventNotification;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.type.CollectionType;
import com.fasterxml.jackson.databind.type.TypeFactory;
import java.io.IOException;
import java.net.URLDecoder;
import java.util.List;
import javax.annotation.Resource;
import lombok.extern.slf4j.Slf4j;
import org.springframework.cloud.aws.messaging.listener.annotation.SqsListener;
import org.springframework.stereotype.Component;
import site.availability.core.bot.model.BotWorkload;
import site.availability.core.bot.processor.WebCrawler;

@Component
@Slf4j
public class WebCrawlMessageConsumer {

  @Resource
  private WebCrawler webCrawler;
  @Resource
  private AmazonS3 amazonS3;
  @Resource
  private ObjectMapper objectMapper;
  private CollectionType typeReference = TypeFactory.defaultInstance()
      .constructCollectionType(List.class, BotWorkload.class);

  @SqsListener(value = "${webcrawl.workload.queue}")
  public void listenToCrawlPayload(S3EventNotification s3EventNotification) {
    log.info("Processing workload message: {}", s3EventNotification.toString());
    s3EventNotification.getRecords().forEach(s3EventNotificationRecord -> {
      String bucketName = s3EventNotificationRecord.getS3().getBucket().getName();
      String key = s3EventNotificationRecord.getS3().getObject().getKey();
      String content = amazonS3.getObjectAsString(bucketName, URLDecoder.decode(key));
      try {
        List<BotWorkload> botWorkloads = objectMapper.readValue(content, typeReference);
        webCrawler.crawl(botWorkloads);
      } catch (IOException e) {
        log.error("error while crawling {}", e.getLocalizedMessage(), e);
      }
    });
  }

}
