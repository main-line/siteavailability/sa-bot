package site.availability.core.bot.config;

import org.asynchttpclient.AsyncHttpClient;
import org.asynchttpclient.DefaultAsyncHttpClientConfig;
import org.asynchttpclient.Dsl;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class HttpClientConfig {

  @Bean
  public DefaultAsyncHttpClientConfig.Builder defaultAsyncHttpClientConfig() {
    return Dsl.config()
//        .setUseNativeTransport(true)
        .setFollowRedirect(true);
  }


  @Bean
  public AsyncHttpClient asyncHttpClient() {
    return Dsl.asyncHttpClient(defaultAsyncHttpClientConfig());
  }

}
