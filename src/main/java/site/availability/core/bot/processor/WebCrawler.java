package site.availability.core.bot.processor;

import java.time.Instant;
import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import javax.annotation.Resource;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import org.asynchttpclient.AsyncHttpClient;
import org.asynchttpclient.ListenableFuture;
import org.asynchttpclient.Response;
import org.springframework.stereotype.Component;
import site.availability.core.bot.model.BotWorkload;
import site.availability.core.bot.model.WebsiteAvailability;
import site.availability.core.bot.queue.BotQueues;

@Component
@Slf4j
public class WebCrawler {

  @Resource
  @Setter
  private AsyncHttpClient asyncHttpClient;

  private static ExecutorService executorService = Executors.newCachedThreadPool();

  private static void accept(ListenableFuture<Response> responseFuture, BotWorkload botWorkload) {
    responseFuture.addListener(() -> {
      Response response = null;
      try {
        response = responseFuture.get();
      } catch (InterruptedException | ExecutionException e) {
        log.info("error while request domain name is {}", botWorkload.getUrl());
      }
      enqueue(botWorkload, response);
    }, executorService);
  }

  private static void enqueue(BotWorkload botWorkload, Response response) {
    int responseCode = (response != null ? response.getStatusCode() : -1);
    String redirect = (response != null ? response.getUri().toUrl() : null);

    BotQueues.WEB_CRAWL_RESPONSE_QUEUE.add(WebsiteAvailability
        .builder()
        .code(responseCode)
        .start(Instant.now())
        .redirect(redirect)
        .url(botWorkload.getUrl())
        .build());
  }

  public void crawl(List<BotWorkload> listOfBotWorkloads) {
    listOfBotWorkloads
        .forEach(botWorkload -> {
          ListenableFuture<Response> responseListenableFuture = asyncHttpClient
              .prepareGet("http://" + botWorkload.getUrl())
              .execute();
          accept(responseListenableFuture, botWorkload);
        });
  }
}
