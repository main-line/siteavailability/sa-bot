package site.availability.core.bot.model;

public interface SQSName {

  public static final String SA_BOT_WORKLOAD_QUEUE = "sa-bot-workload-queue";
}
